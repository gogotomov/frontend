FROM node:latest as build

WORKDIR /home/node/

ARG API_ROOT="https://conduit.productionready.io/api"
#ARG DEFAULT_API_URL#
#ENV API_ROOT=${DEFAULT_API_URL:-"https://conduit.productionready.io/api"}

COPY ./ ./

RUN  sed -i "s|https://conduit.productionready.io/api|${API_ROOT}|" /home/node/src/agent.js \
    && npm install \ && npm run build

FROM nginx:latest

COPY --from=build /home/node/build/ /usr/share/nginx/html/

EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

