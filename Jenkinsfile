pipeline {
agent {
		node {
			label 'jenkins-slave-1'
			 }
	  }
tools {nodejs "nodejs"}

parameters {
        booleanParam(name: "TEST_BOOLEAN", defaultValue: true, description: "Sample boolean parameter")
        string(name: "TEST_STRING", defaultValue: "ssbostan", trim: true, description: "Sample string parameter")
        text(name: "TEST_TEXT", defaultValue: "Jenkins Pipeline Tutorial", description: "Sample multi-line text parameter")
        choice(name: "TEST_CHOICE", choices: ["production", "staging", "development"], description: "Sample multi-choice parameter")
    }

environment {
        VERSION = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}-develop-${BUILDS_TODAY}');
        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "10.0.3.185:8081"
        NEXUS_REPOSITORY = "maven-nexus-repo"
        NEXUS_CREDENTIAL_ID = "nexus-user-credentials"
		}
	stages{	
		stage("Clone code from Bitbucket") {
            steps {
                cleanWs()
                git branch: 'master',  
				url: 'https://gogotomov@bitbucket.org/gogotomov/frontend.git';
                }
            }
		stage('Code Quality Check via SonarQube') {
				steps {
					script {
						def scannerHome = tool 'sonarqube';
							withSonarQubeEnv("sonarqube") {
							sh "${tool("sonarqube")}/bin/sonar-scanner \
							-Dsonar.projectKey=jenkins-project \
							-Dsonar.host.url=http://10.0.3.53:9000 \
							-Dsonar.sources=."
						}
				}
		}
   }   
		stage("Quality gate") {
			steps {
				script {
					def qualitygate = waitForQualityGate()
					sleep(10)
					if (qualitygate.status != "OK") {
					waitForQualityGate abortPipeline: true
          }
        }
      }
	}
		stage ('Install Project Dependencies') {
            steps {
                nodejs(nodeJSInstallationName: 'nodejs') {
                sh 'npm install'
                }
            }
        }
		stage('Build') {
		  when {
		     branch 'develop'
			}
            steps {
                script {
                    echo "BUILDING..."
                    sh 'npm run build'
					echo "ZIP the BUILD"
					sh 'zip -r back-bundle-${BUILD_NUMBER}.zip build/* build/static/js/*'
                }
            }    
        }
		stage("Publish to Nexus Repository Manager") {
		  when {
		     branch 'develop'
			}
            steps {
                script {           
                    nexusArtifactUploader artifacts: [[artifactId: 'builds', classifier: '', file: 'back-bundle-${BUILD_NUMBER}.zip', type: 'zip']],
                    credentialsId: NEXUS_CREDENTIAL_ID,
                    groupId: 'conduit',
                    nexusUrl: NEXUS_URL,
                    nexusVersion: NEXUS_VERSION,
                    protocol: NEXUS_PROTOCOL,
                    repository: NEXUS_REPOSITORY,
                    version: VERSION
                    } 
                }
            }
        }
       
}